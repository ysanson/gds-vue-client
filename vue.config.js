//const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
//  .BundleAnalyzerPlugin;
const MomentTimezoneDataPlugin = require("moment-timezone-data-webpack-plugin");
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

module.exports = {
  configureWebpack: {
    plugins: [
      //new BundleAnalyzerPlugin(),
      new MomentTimezoneDataPlugin({
        matchZones: "Indian/Reunion",
        startYear: 2000
      }),
      new MomentLocalesPlugin({
        localesToKeep: ["fr-fr"]
      })
    ]
  }
};
