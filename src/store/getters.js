export default {
  layout(state) {
    return state.layout;
  },
  isAuthenticated: state => !!state.token,
  currentUser: state => {
    if (state.user !== "") {
      const user = JSON.parse(state.user);
      return user;
    } else {
      return "";
    }
  },
  darkTheme: state => state.darkTheme
};
