export default {
  layout: "main-layout",
  token: localStorage.getItem("user-token") || "",
  user: localStorage.getItem("user") || "",
  darkTheme: false,
  speciesNames: [],
  racesNames: [],
  natspanNames: [],
  indic1: [],
  indic2: [],
  otherINFID: [],
  deathCauses: []
};
