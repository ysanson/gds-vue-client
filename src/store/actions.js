import Axios from "axios";

const actions = {
  logout({ commit }) {
    return new Promise(resolve => {
      localStorage.removeItem("user-token");
      localStorage.removeItem("user");
      commit("DELETE_LOGIN");
      resolve();
    });
  },
  getSpeciesNames({ commit, state }) {
    return new Promise((resolve, reject) => {
      if (state.speciesNames !== []) resolve();
      Axios.get(process.env.VUE_APP_API_SERVICE + "species")
        .then(Response => {
          const data = [];
          for (const species of Response.data) {
            data.push({
              text: species.Nom_Espece,
              value: species.RefEspece
            });
          }
          commit("SET_SPECIES", data);
          resolve();
        })
        .catch(ErrorEvent => reject(ErrorEvent));
    });
  },
  getRacesNames({ commit, state }) {
    return new Promise((resolve, reject) => {
      if (state.racesNames !== []) resolve();
      Axios.get(process.env.VUE_APP_API_SERVICE + "races", {
        params: {
          onlyCows: true
        }
      })
        .then(Response => {
          const data = [];
          for (const race of Response.data) {
            data.push({
              text: race.NomRace,
              value: race["N°"]
            });
          }
          commit("SET_RACES", data);
          resolve();
        })
        .catch(ErrorEvent => reject(ErrorEvent));
    });
  },
  getNatspan({ commit, state }) {
    return new Promise((resolve, reject) => {
      if (state.natspanNames !== []) resolve();
      Axios.get(process.env.VUE_APP_API_SERVICE + "natspan")
        .then(Response => {
          const data = [];
          for (const nat of Response.data) {
            data.push({
              text: nat.Espece + " : " + nat.Libelle + " (" + nat.Memo + ")",
              value: nat.Id
            });
          }
          commit("SET_NATSPAN", data);
          resolve();
        })
        .catch(ErrorEvent => reject(ErrorEvent));
    });
  },
  getINFID({ commit, state }) {
    return new Promise((resolve, reject) => {
      if (state.indic1 !== [] && state.indic2 !== [] && state.otherINFID !== [])
        resolve();
      Axios.get(process.env.VUE_APP_API_SERVICE + "infid/specific", {
        params: { exceptCows: true }
      })
        .then(Response => {
          const { otherINFID, indic1, indic2 } = Response.data;
          var data = [];
          for (const infid of otherINFID) {
            data.push({
              text: infid.espece + " : " + infid.libelle,
              value: infid.id
            });
          }
          commit("SET_INFID", data);
          data = [];
          for (const ind1 of indic1) {
            data.push({
              text: ind1.Libelle,
              value: ind1.Id
            });
          }
          commit("SET_IND1", data);
          data = [];
          for (const ind2 of indic2) {
            data.push({
              text: ind2.Libelle,
              value: ind2.Id
            });
          }
          commit("SET_IND2", data);
          resolve();
        })
        .catch(ErrorEvent => reject(ErrorEvent));
    });
  },
  getDeathCauses({ commit, state }) {
    return new Promise((resolve, reject) => {
      if (state.deathCauses !== []) resolve();
      Axios.get(process.env.VUE_APP_API_SERVICE + "deathCauses")
        .then(Response => {
          const data = Response.data.map(item => {
            return {
              text: item.cause_spe + " (" + item.trouble_sante + ")",
              value: item.id
            };
          });
          commit("SET_DEATHCAUSES", data);
          resolve();
        })
        .catch(ErrorEvent => reject(ErrorEvent));
    });
  }
};

export default actions;
