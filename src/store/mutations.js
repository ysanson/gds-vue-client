export default {
  SET_LAYOUT(state, payload) {
    state.layout = payload;
  },
  UPDATE_USER(state, payload) {
    localStorage.setItem("user", JSON.stringify(payload));
    state.user = localStorage.getItem("user");
  },
  UPDATE_TOKEN(state, payload) {
    localStorage.setItem("user-token", payload);
    state.token = localStorage.getItem("user-token");
  },
  DELETE_LOGIN(state) {
    state.user = "";
    state.token = "";
  },
  DARK_THEME(state, payload) {
    if (payload) {
      state.darkTheme = true;
    } else {
      state.darkTheme = false;
    }
  },
  SET_SPECIES(state, payload) {
    state.speciesNames = [];
    state.speciesNames = payload;
  },
  SET_RACES(state, payload) {
    state.racesNames = [];
    state.racesNames = payload;
  },
  SET_NATSPAN(state, payload) {
    state.natspanNames = [];
    state.natspanNames = payload;
  },
  SET_INFID(state, payload) {
    state.otherINFID = [];
    state.otherINFID = payload;
  },
  SET_IND1(state, payload) {
    state.indic1 = [];
    state.indic1 = payload;
  },
  SET_IND2(state, payload) {
    state.indic2 = [];
    state.indic2 = payload;
  },
  SET_CALLID(state, payload) {
    state.callId = payload;
  },
  SET_DEATHCAUSES(state, payload) {
    state.deathCauses = [];
    state.deathCauses = payload;
  }
};
