import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import moment from "moment";

function createRoadSheet(roadData, zone) {
  pdfMake.vfs = pdfFonts.pdfMake.vfs;
  const date = new Date().toLocaleDateString();
  const tomorrow = moment()
    .locale("fr-fr")
    .add(1, "days")
    .format("DD MMMM YYYY");
  roadData.unshift([
    { text: "Appel", style: "tableHeader" },
    { text: "Ref", style: "tableHeader" },
    { text: "Éleveur", style: "tableHeader" },
    { text: "Téléphone", style: "tableHeader" },
    { text: "Adresse", style: "tableHeader" },
    { text: "Animaux", style: "tableHeader" },
    { text: "Remarques", style: "tableHeader" },
    { text: "Accord véto", style: "tableHeader" }
  ]);
  const docDefinition = {
    pageOrientation: "landscape",
    footer: function(currentPage, pageCount) {
      return currentPage.toString() + " sur " + pageCount;
    },
    content: [
      { text: "Une seule LETTRE DE VOITURE", style: "centerize" },
      {
        text:
          "Article 5 arrêté du 9 novembre 1999 relatif aux documents de transport devant se trouver à bord des véhicules de transport routier de marchandises",
        style: "legal"
      },
      { text: "Date d'établissement: " + date, style: "centerize" },
      {
        canvas: [
          {
            type: "line",
            x1: 0,
            y1: 20,
            x2: 760,
            y2: 20,
            lineWidth: 3
          }
        ]
      },
      {
        text: "Transporteur (Vehicule ____________________)",
        style: "centerize",
        lineHeight: 2
      },
      { text: "Nom : SARL GDS RUN SERVICES", style: "centerize" },
      {
        text: "Adresse : 1 RUE DU PÈRE HAUCK - 97418 LA PLAINE DES CAFRES",
        style: "centerize"
      },
      { text: "SIREN : 531080133", style: "centerize", lineHeight: 2 },
      { text: "Date de prise en charge : " + tomorrow, style: "legal" },
      {
        text:
          "Heure de la première prise en charge: __H__                                                                                                                               Heure de départ : 06 H 15"
      },
      {
        style: "bodyless",
        table: {
          widths: ["*"],
          headerRows: 1,
          body: [
            [
              {
                text: "Journée d'enlèvement du " + tomorrow,
                style: "tableHeader"
              }
            ],
            [
              {
                text:
                  "IDENTIFICATION TOURNÉE : " +
                  moment()
                    .add(1, "days")
                    .format("MMDD - YYYY") +
                  " / ZONE: " +
                  zone,
                style: "tableHeader"
              }
            ]
          ]
        }
      },
      {
        table: {
          headerRows: 1,
          widths: ["auto", "auto", "auto", "*", "auto", "auto", "*", "auto"],
          body: roadData
        }
      },
      {
        text: "Destinataire",
        style: "header",
        lineHeight: 2,
        pageBreak: "before"
      },
      {
        text:
          "Total kg : ______              Date : ____/____/_______     Nom / Signature et cachet",
        style: "right"
      },
      { text: "SICA DES SABLES", style: "bold" },
      { text: "ZAC Des Sables", style: "bold" },
      { text: "Rue Michel Debré", style: "bold" },
      { text: "97427 Étang-Salé", style: "bold" },
      {
        text:
          "Tel : 0262 55 44 8    Fax : 0262 55 40 52   Contact : Gérard MARCK"
      }
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        margin: [0, 0, 0, 10]
      },
      subheader: {
        fontSize: 13,
        bold: true,
        margin: [0, 10, 0, 5]
      },
      tableExample: {
        margin: [0, 5, 0, 15]
      },
      tableHeader: {
        bold: true,
        fontSize: 13,
        color: "black",
        alignment: "center"
      },
      centerize: {
        alignment: "center"
      },
      legal: {
        fontSize: 9,
        italics: true,
        alignment: "center"
      },
      bodyless: {
        margin: [10, 10, 0, 10]
      }
    },
    defaultStyle: {
      // alignment: 'justify'
    }
  };
  const docName = "E_Planning_Chauffeurs_" + zone.toUpperCase() + ".pdf";
  pdfMake.createPdf(docDefinition).download(docName);
}

function createFarmAnimalsSheet(animals, ede, siret, farmName) {
  pdfMake.vfs = pdfFonts.pdfMake.vfs;
  animals.unshift([
    { text: "N° Appel", style: "subheader" },
    { text: "Date & Heure", style: "subheader" },
    { text: "Date Enlèvement", style: "subheader" },
    { text: "N° Certif.", style: "subheader" },
    { text: "Espèce", style: "subheader" },
    { text: "Sexe", style: "subheader" },
    { text: "Nombre", style: "subheader" },
    { text: "Poids", style: "subheader" },
    { text: "Causes de la mort", style: "subheader" },
    { text: "Identification", style: "subheader" },
    { text: "N° DAB", style: "subheader" },
    { text: "N° ASDA", style: "subheader" }
  ]);
  const docDefinition = {
    pageOrientation: "landscape",
    footer: function(currentPage, pageCount) {
      return currentPage.toString() + " sur " + pageCount;
    },
    content: [
      {
        text: "Liste des collectes pour " + farmName,
        style: ["header", "centerize"]
      },
      { text: "EDE : " + ede, style: "subheader" },
      { text: "SIRET : " + siret, style: "subheader" },
      {
        text: "Date d'établissement : " + new Date().toLocaleString(),
        style: "legal"
      },
      {
        table: {
          headerRows: 1,
          //widths: ["auto", "auto", "auto", "*", "auto", "auto", "*", "auto"],
          body: animals
        }
      }
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        margin: [0, 0, 0, 10]
      },
      subheader: {
        fontSize: 13,
        bold: true,
        margin: [0, 10, 0, 5]
      },
      tableHeader: {
        bold: true,
        fontSize: 13,
        color: "black",
        alignment: "center"
      },
      centerize: {
        alignment: "center"
      },
      legal: {
        fontSize: 9,
        italics: true,
        alignment: "center"
      },
      bodyless: {
        margin: [10, 10, 0, 10]
      }
    },
    defaultStyle: {
      // alignment: 'justify'
    }
  };
  pdfMake.createPdf(docDefinition).download("Relevé " + farmName + ".pdf");
}

export default {
  createRoadSheet,
  createFarmAnimalsSheet
};
