import Vue from "vue";
import axios from "axios";
import "./plugins/vuetify";

import AsyncComputed from "vue-async-computed";
import VuePageTitle from "vue-page-title";
import JsonExcel from "vue-json-excel";
import router from "./router";
import store from "./store";

import App from "./App.vue";

Vue.config.productionTip = false;

const token = localStorage.getItem("user-token");
if (token) {
  axios.defaults.headers.common["Authorization"] = token;
}

axios.interceptors.response.use(
  Response => {
    return Response;
  },
  ErrorEvent => {
    if (ErrorEvent.response.status === 401) {
      console.log("Not authenticated");
      store
        .dispatch("logout")
        .then(() => router.push("/login"))
        .catch(error => console.log(error))
        .finally(() => Promise.reject(ErrorEvent));
    } else {
      Promise.reject(ErrorEvent);
    }
  }
);

Vue.use(AsyncComputed);
Vue.use(VuePageTitle, {
  suffix: " - GDS"
});
Vue.component("downloadExcel", JsonExcel);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
