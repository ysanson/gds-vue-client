import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";
import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    primary: colors.green.darken1,
    secondary: "#03a9f4",
    accent: "#ffc107",
    error: "#f44336",
    warning: "#ff9800",
    info: "#009688",
    success: "#4caf50"
  }
});
