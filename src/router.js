import Vue from "vue";
import Router from "vue-router";

import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Farms from "./views/farms/Farms.vue";
import FarmCRUD from "./views/farms/FarmCRUD.vue";
import FarmDetailsView from "./views/farms/FarmDetailsView.vue";
import FarmAnimals from "./views/farms/FarmAnimals.vue";

import Calls from "./views/calls/Calls.vue";
const CallCreate = () =>
  import(/*webpackChunkName: "callCreate"*/ "./views/calls/CallCreate.vue");
import CallEdit from "./views/calls/CallEdit.vue";
import CallDetails from "./views/calls/CallDetailsView.vue";

import Tours from "./views/tours/Tours.vue";
import TourCRUD from "./views/tours/TourCRUD.vue";
import TourDetails from "./views/tours/TourDetailsView.vue";
import Planning from "./views/planning/Planning.vue";
import PlanningZone from "./views/planning/PlanningZone.vue";

import Users from "./views/users/Users.vue";
import UserCRUD from "./views/users/UserCRUD.vue";
import UserDetails from "./views/users/UserDetails.vue";
import UserPwdEdit from "./views/users/UserPasswordEdit.vue";

import Anomalies from "./views/anomalies/Anomalies.vue";

import Reports from "./views/reports/Reports.vue";
import CallRemoved from "./views/reports/CallsRemoved.vue";
import VetDashboard from "./views/dashboards/VetDashboard.vue";

const Error403 = () =>
  import(/*webpackChunkName: "error403"*/ "./views/errors/Error403.vue");

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/login/",
      name: "login",
      component: Login,
      props: true
    },
    {
      path: "/eleveurs",
      name: "farms",
      component: Farms,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/eleveurs/nouveau",
      name: "farmCreate",
      component: FarmCRUD,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/eleveurs/modifier/:farmId",
      name: "farmEdit",
      component: FarmCRUD,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/eleveurs/:farmId/animaux",
      name: "farmAnimals",
      component: FarmAnimals,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/eleveurs/:farmId",
      name: "farmDetails",
      component: FarmDetailsView,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/appels",
      name: "calls",
      component: Calls,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/appels/nouveau",
      name: "callCreate",
      component: CallCreate,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/appels/modifier/:callId",
      name: "callEdit",
      component: CallEdit,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/appels/:callId",
      name: "CallDetails",
      component: CallDetails,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/tournees",
      name: "tours",
      component: Tours,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/tournees/nouveau",
      name: "TourCreate",
      component: TourCRUD,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/tournees/modifier/:tourId",
      name: "TourEdit",
      component: TourCRUD,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/tournees/:tourId",
      name: "TourDetails",
      component: TourDetails,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/planning",
      name: "planning",
      component: Planning,
      meta: {
        requiresAuth: true,
        rights: 2
      }
    },
    {
      path: "/planning/:zoneCode",
      name: "planningZone",
      component: PlanningZone,
      meta: {
        requiresAuth: true,
        rights: 2
      }
    },
    {
      path: "/utilisateurs",
      name: "users",
      component: Users,
      meta: {
        requiresAuth: true,
        rights: 4
      }
    },
    {
      path: "/utilisateurs/nouveau",
      name: "userCreate",
      component: UserCRUD,
      meta: {
        requiresAuth: true,
        rights: 4
      }
    },
    {
      path: "/utilisateurs/editer/:userId",
      name: "userEdit",
      component: UserCRUD,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/utilisateurs/password",
      name: "userPwd",
      component: UserPwdEdit,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/utilisateurs/:userId",
      name: "userDetails",
      component: UserDetails,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/anomalies",
      name: "anomalies",
      component: Anomalies,
      meta: {
        requiresAuth: true,
        rights: 3
      }
    },
    {
      path: "/rapports",
      name: "reports",
      component: Reports,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/rapports/appels-enleves",
      name: "callRemoved",
      component: CallRemoved,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: VetDashboard,
      meta: {
        requiresAuth: true,
        rights: 1
      }
    },
    {
      path: "/403",
      name: "Error 403",
      component: Error403
    }
  ]
});

router.beforeEach((to, from, next) => {
  // If the user is registered
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("user-token") == null) {
      //If not authenticated
      next({
        path: "/login",
        params: { nextUrl: to.path || "/" }
      });
    } else {
      let user = JSON.parse(localStorage.getItem("user"));
      // We test if the user is admin
      if (to.matched.some(record => record.meta.rights)) {
        if (user.role >= to.meta.rights) {
          next();
        } else {
          next({
            path: "/403"
          });
        }
      } else {
        next();
      }
    }
  } else {
    next();
  }
});

export default router;
