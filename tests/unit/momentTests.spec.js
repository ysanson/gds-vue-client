import moment from "moment-timezone";
import { expect } from "chai";

describe("momentjs", () => {
  it("renders date correctly", () => {
    const currentHour = new Date().toLocaleTimeString();
    const date = new Date();
    const momentDate = moment
      .tz(currentHour, "HH:mm:ss", "Indian/Reunion")
      .toDate();
    expect(momentDate.toUTCString()).to.equal(date.toUTCString());
  });
});
